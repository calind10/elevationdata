#pragma once
#include <vector>

namespace elevation
{
	class ElevationImage
	{
	public:
		static constexpr int16_t kUnknownValue = -9999;

		ElevationImage(unsigned int width, unsigned int height);

		unsigned int width() const { return m_width; }
		unsigned int height() const { return m_height; }
		const int16_t* data() const { return m_data.data(); }

		int16_t elevation(unsigned int w, unsigned int h) const;
		void setElevation(unsigned int w, unsigned int h, int16_t elevation);

		static ElevationImage fromHGTData(unsigned int w, unsigned int h, const unsigned char* data);

	private:
		auto index(unsigned int w, unsigned int h) const
		{
			return w * m_height + h;
		}

		unsigned int m_width;
		unsigned int m_height;
		std::vector<int16_t> m_data;
	};
}