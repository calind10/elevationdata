#pragma once

#include <QObject>
#include "ElevationImage.h"
#include <memory>

namespace viewer
{
	class ElevationImageWrapper : public QObject
	{
		Q_OBJECT
	public:
		std::shared_ptr<elevation::ElevationImage> currentImage() const
		{
			return m_image;
		}

		void setCurrentImage(std::shared_ptr<elevation::ElevationImage> img)
		{
			m_image = std::move(img);
		}

	private:
		std::shared_ptr<elevation::ElevationImage> m_image;
	};
}