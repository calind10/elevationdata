
LIST(APPEND CMAKE_PREFIX_PATH $ENV{QT_DIR})

find_package(Qt5 COMPONENTS Core Qml Gui Quick REQUIRED)

add_executable(Viewer
	main.cpp
	QmlRegistrar.cpp
	QmlRegistrar.h
	ElevationImage.h
	ElevationImage.cpp
	ElevationImageWrapper.h
	views/ElevationImageItem.h
	views/ElevationImageItem.cpp
	views/ElevationImageRender.h
	views/ElevationImageRender.cpp
	view_model/MainViewModel.h
	view_model/MainViewModel.cpp
	view_model/DisplayViewModel.h
	view_model/DisplayViewModel.cpp
	resources.qrc)

target_link_libraries(Viewer
	PRIVATE Qt5::Qml
			Qt5::Gui
			Qt5::Quick)

target_include_directories(Viewer 
	PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})