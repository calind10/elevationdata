#include "ElevationImage.h"
#include <cassert>

namespace elevation
{
	ElevationImage::ElevationImage(unsigned int w, unsigned int h)
		: m_width(w), m_height(h)
	{
		m_data.resize(m_width * m_height);
	}

	int16_t ElevationImage::elevation(unsigned int w, unsigned int h) const
	{
		const auto actualIndex = index(w, h);
		if (w >= m_width || h >= m_height || actualIndex >= m_data.size())
			return kUnknownValue;
		return m_data[actualIndex];
	}

	void ElevationImage::setElevation(unsigned int w, unsigned int h, int16_t elevation)
	{
		assert(w < m_width);
		assert(h < m_height);

		m_data[index(w, h)] = elevation;
	}

	ElevationImage ElevationImage::fromHGTData(unsigned int w, unsigned int h, const unsigned char* data)
	{
		ElevationImage result(w, h);
		for (unsigned int i = 0; i < w; ++i)
			for (unsigned int j = 0; j < h; ++j)
			{
				result.m_data[result.index(i, j)] = data[0] << 8 | data[1];
				data += 2;
			}
		return result;
	}
}