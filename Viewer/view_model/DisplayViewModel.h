#pragma once
#include <QObject>
#include "ElevationImage.h"
#include "ElevationImageWrapper.h"

namespace viewer
{
	class DisplayViewModel : public QObject
	{
		Q_OBJECT
		Q_PROPERTY(viewer::ElevationImageWrapper* imageData READ currentImage NOTIFY elevationImageChanged)
	public:
		explicit DisplayViewModel(QObject* parent = nullptr);

		void changeElevationImage(elevation::ElevationImage image);
		ElevationImageWrapper* currentImage()
		{
			return &m_currentImage;
		}

	Q_SIGNALS:
		void elevationImageChanged();

	private:
		ElevationImageWrapper m_currentImage;
	};
}