#include "MainViewModel.h"
#include <QtQml>
#include <cassert>
#include "QmlRegistrar.h"
#include "ElevationImage.h"

CREATE_QML_TYPE_REGISTER(
	qmlRegisterType<viewer::MainViewModel>();
	)

namespace viewer
{
	MainViewModel::MainViewModel(QObject* parent)
		: QObject(parent)
	{
	}

	void MainViewModel::loadElevationFile(const QUrl& fileUrl)
	{
		assert(fileUrl.isLocalFile());
		QFile file(fileUrl.toLocalFile());
		if (!file.open(QIODevice::ReadOnly))
		{
			//todo: show error msg
			return;
		}

		const QByteArray& fileData = file.readAll();
		const unsigned int elevationWidth = 1201; //todo: check if there are different size hgt
		const unsigned int elevationHeight = 1201;
		assert(fileData.length() == elevationHeight * elevationWidth * 2);

		auto result = elevation::ElevationImage::fromHGTData(elevationWidth, elevationHeight, reinterpret_cast<const unsigned char*>(fileData.data()));
		m_displayVM.changeElevationImage(std::move(result));
	}
}