#include "DisplayViewModel.h"
#include <QtQml>
#include "QmlRegistrar.h"

CREATE_QML_TYPE_REGISTER(
	qmlRegisterType<viewer::DisplayViewModel>(viewer::kQmlNamespace, 1, 0, "ImageDisplay");
	qmlRegisterType<viewer::ElevationImageWrapper>();
)

namespace viewer
{
	DisplayViewModel::DisplayViewModel(QObject* parent /*= nullptr*/)
		: QObject(parent)
	{
	}

	void DisplayViewModel::changeElevationImage(elevation::ElevationImage image)
	{
		m_currentImage.setCurrentImage(std::make_shared<elevation::ElevationImage>(std::move(image)));
		emit elevationImageChanged();
	}
}