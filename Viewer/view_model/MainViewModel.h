#pragma once

#include <QObject>
#include "DisplayViewModel.h"

namespace viewer
{
	class MainViewModel : public QObject
	{
		Q_OBJECT
		Q_PROPERTY(DisplayViewModel* displayVM READ getDisplayVM)
	public:
		explicit MainViewModel(QObject* parent = nullptr);

		Q_SLOT void loadElevationFile(const QUrl& url);

		DisplayViewModel* getDisplayVM() { return &m_displayVM; }

	private:
		DisplayViewModel m_displayVM;
	};
}