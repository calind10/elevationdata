#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "QmlRegistrar.h"
#include "view_model/MainViewModel.h"

int main(int argc, char** argv)
{
	QGuiApplication app(argc, argv);

	QQmlApplicationEngine engine;
	viewer::QmlRegistrar::instance().runRegistration();
	viewer::MainViewModel mainViewModel;
	engine.rootContext()->setContextProperty("mainVM", &mainViewModel);
	engine.load(QUrl(QStringLiteral("qrc:/views/main.qml")));

	return app.exec();
}