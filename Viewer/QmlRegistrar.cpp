#include "QmlRegistrar.h"
#include <assert.h>

namespace viewer
{
	QmlRegistrar& QmlRegistrar::instance()
	{
		static QmlRegistrar instance;
		return instance;
	}

	void QmlRegistrar::add(QmlRegistrar::RegistrarType registrar)
	{
		m_registrars.push_back(registrar);
	}

	void QmlRegistrar::runRegistration()
	{
		for (const auto& reg : m_registrars)
		{
			reg();
		}
		m_registrars.clear();
	}
}