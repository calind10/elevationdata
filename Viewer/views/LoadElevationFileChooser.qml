import QtQuick 2.5
import QtQuick.Dialogs 1.2

FileDialog {
    title: "Choose elevation file to load"
    nameFilters: [ "elevation data (*.hgt)" ]
    selectMultiple: false
    visible: false
}
