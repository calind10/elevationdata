#include "ElevationImageRender.h"
#include <QtQml>
#include <QOpenGLShaderProgram>
#include "QmlRegistrar.h"
#include "ElevationImage.h"

namespace
{
	namespace GlAttributes
	{
		enum
		{
			position = 0,
			texture = 1
		};
	}

	namespace GlUniforms
	{
		enum
		{
			colorScale = 0,
			elevationTexture,
		};
	}

	constexpr const char* kUniformNames[] =
	{
		"colorScale",
		"elevationTexture",
	};

	const elevation::ElevationImage kBackgroundImage = []()
	{
		std::vector<unsigned char> imageData;
		imageData.push_back(0);
		imageData.push_back(0);

		return elevation::ElevationImage::fromHGTData(1, 1, imageData.data());
	}();
}

namespace viewer
{
	ElevationImageRender::ElevationImageRender(QObject* parent /*= nullptr*/)
		: QObject(parent)
		, m_imageHandle(0)
		, m_imageChanged(false)
	{
	}

	ElevationImageRender::~ElevationImageRender()
	{
		releaseCurrentImage();
	}

	bool ElevationImageRender::isGLInitialized() const
	{
		return static_cast<bool>(m_program);
	}

	void ElevationImageRender::initializeGL()
	{
		initializeOpenGLFunctions();
		m_uniformLocations.clear();

		m_program.reset(new QOpenGLShaderProgram());
		m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex,
			R"(
			in vec4 coordinates;
			in vec2 texCoords;
			void main()
			{
				gl_Position = coordinates;
				gl_TexCoord[0].st = texCoords;
			}
			)");
		m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment,
			R"(
				uniform sampler2D elevationTexture;
				uniform float colorScale;
				void main()
				{
					float outColor = texture2D(elevationTexture, gl_TexCoord[0].st).r * colorScale;
					gl_FragColor = vec4(outColor);
				}
			)");
		m_program->bindAttributeLocation("coordinates", GlAttributes::position);
		m_program->bindAttributeLocation("texCoords", GlAttributes::texture);
		m_program->link();

		for (const auto& uniformName : kUniformNames)
		{
			m_uniformLocations.push_back(m_program->uniformLocation(uniformName));
		}
	}

	void ElevationImageRender::render()
	{
		// OpenGL initialization needs to be done here, on the rendering thread
		if (!isGLInitialized())
			initializeGL();

		// take ownership over the rendered image, so it doesn't change during rendering
		std::shared_ptr<elevation::ElevationImage> currentImage;
		bool reuploadImage = false;
		{
			std::lock_guard<std::mutex> lock(m_imageSync);
			currentImage = m_image;
			reuploadImage = m_imageChanged;
			m_imageChanged = false;
		}

		glViewport(m_viewport.x(), m_viewport.y(), m_viewport.width(), m_viewport.height());
		glDisable(GL_DEPTH_TEST);
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT);

		if (currentImage)
		{
			renderImage(*currentImage, reuploadImage);
		}
		else
		{
			renderImage(kBackgroundImage, true);
		}

		emit renderingComplete();
	}

	void ElevationImageRender::setViewport(QRect viewport)
	{
		m_viewport = std::move(viewport);
	}

	void ElevationImageRender::setImage(std::shared_ptr<elevation::ElevationImage> image)
	{
		std::lock_guard<std::mutex> lock(m_imageSync);
		m_image = image;
		m_imageChanged = true;
	}

	void ElevationImageRender::uploadImage(const elevation::ElevationImage& img)
	{
		if (m_imageHandle == 0)
		{
			glGenTextures(1, &m_imageHandle);
		}
		bindCurrentTexture();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, img.width(), img.height(), 0, GL_RED, GL_SHORT, img.data());
	}

	void ElevationImageRender::releaseCurrentImage()
	{
		if (m_imageHandle)
			glDeleteTextures(1, &m_imageHandle);
	}

	void ElevationImageRender::bindCurrentTexture()
	{
		glEnable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_imageHandle);
		m_program->setUniformValue(m_uniformLocations[GlUniforms::elevationTexture], 0);

		//todo make this configurable
		const float scaleFactor = std::numeric_limits<int16_t>::max() / 5895.0f;
		m_program->setUniformValue(m_uniformLocations[GlUniforms::colorScale], scaleFactor);
	}

	void ElevationImageRender::renderImage(const elevation::ElevationImage& image, bool upload)
	{
		m_program->bind();
		m_program->enableAttributeArray(GlAttributes::position);
		m_program->enableAttributeArray(GlAttributes::texture);

		if (m_imageHandle == 0 || upload)
		{
			uploadImage(image);
		}
		else
		{
			bindCurrentTexture();
		}

		static constexpr float positions[] =
		{
			-1, -1,
			 1, -1,
			-1,  1,
			 1,  1
		};
		static constexpr float texture[] =
		{
			0, 0,
			1, 0,
			0, 1,
			1, 1
		};
		m_program->setAttributeArray(GlAttributes::position, GL_FLOAT, positions, 2);
		m_program->setAttributeArray(GlAttributes::texture, GL_FLOAT, texture, 2);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		m_program->disableAttributeArray(GlAttributes::texture);
		m_program->disableAttributeArray(GlAttributes::position);
		m_program->release();
	}
}