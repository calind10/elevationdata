import QtQuick 2.5
import QtQuick.Controls 1.4
import elevation.viewer 1.0

Item {
    anchors.fill: parent
    property ImageDisplay model: undefined;
    ElevationImage {
        id: imgRender
        anchors.fill: parent
        imageData: model.imageData
    }
}
