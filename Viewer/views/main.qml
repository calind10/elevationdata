import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 800
    height: 600
    //need to change color to transparent as we it will overwrite our OpenGL renderer
    color: "transparent"

    menuBar: MenuBar {
        Menu {
            title: "&File"
            MenuItem {
                text: "&Load elevation file"
                onTriggered: hgtLoadChooser.visible = true
            }
        }
    }

    toolBar: ToolBar {
        height: 0
    }

    LoadElevationFileChooser {
        id: hgtLoadChooser
        onAccepted: mainVM.loadElevationFile(hgtLoadChooser.fileUrl);
    }

    ImageDisplay {
        model: mainVM.displayVM
    }
}
