#include "ElevationImageItem.h"
#include <QtQml>
#include <QQuickWindow>
#include "QmlRegistrar.h"
#include "ElevationImageRender.h"
#include "ElevationImageWrapper.h"

CREATE_QML_TYPE_REGISTER(
	qmlRegisterType<viewer::ElevationImageItem>(viewer::kQmlNamespace, 1, 0, "ElevationImage");
)

namespace viewer
{
	ElevationImageItem::ElevationImageItem(QQuickItem* parent /*= nullptr*/)
		: QQuickItem(parent)
	{
		connect(this, &QQuickItem::windowChanged, this, &ElevationImageItem::handleWindowChanged);
	}

	void ElevationImageItem::handleWindowChanged(QQuickWindow* window)
	{
		if (window)
		{
			connect(window, &QQuickWindow::beforeSynchronizing, this, &ElevationImageItem::prepareForRendering, Qt::DirectConnection);
			connect(window, &QQuickWindow::sceneGraphInvalidated, this, &ElevationImageItem::cleanup, Qt::DirectConnection);
			window->setClearBeforeRendering(false);
		}
	}

	void ElevationImageItem::prepareForRendering()
	{
		if (!m_renderer)
		{
			m_renderer.reset(new ElevationImageRender());
			connect(window(), &QQuickWindow::beforeRendering, m_renderer.get(), &ElevationImageRender::render, Qt::DirectConnection);
			connect(m_renderer.get(), &ElevationImageRender::renderingComplete, window(), &QQuickWindow::resetOpenGLState, Qt::DirectConnection);
		}

		m_renderer->setViewport(QRect(x(), y(), width(), height()));
	}

	void ElevationImageItem::cleanup()
	{
		m_renderer.reset();
	}

	void ElevationImageItem::setNewImage(const ElevationImageWrapper* img)
	{
		if (m_renderer)
		{
			m_renderer->setImage(img->currentImage());
			window()->update();
		}
	}
}