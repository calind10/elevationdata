#pragma once

#include <QQuickItem>
#include <memory>

namespace viewer
{
	class ElevationImageRender;
	class ElevationImageWrapper;

	class ElevationImageItem : public QQuickItem
	{
		Q_OBJECT
		Q_PROPERTY(viewer::ElevationImageWrapper* imageData READ getImage WRITE setNewImage)
	public:
		explicit ElevationImageItem(QQuickItem* parent = nullptr);
		ElevationImageWrapper* getImage() const { return nullptr; }//ignore this function. this is only for Qt to see

	public Q_SLOTS:
		void setNewImage(const ElevationImageWrapper*);

	private Q_SLOTS:
		void handleWindowChanged(QQuickWindow*);
		void prepareForRendering();
		void cleanup();

	private:
		std::unique_ptr<ElevationImageRender> m_renderer;
	};
}