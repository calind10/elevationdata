#pragma once

#include <QObject>
#include <QRect>
#include <QOpenGLFunctions>
#include <mutex>
#include <memory>
#include <vector>

class QOpenGLShaderProgram;
namespace elevation
{
	class ElevationImage;
}

namespace viewer
{
	class ElevationImageRender : public QObject, protected QOpenGLFunctions
	{
		Q_OBJECT
	public:
		explicit ElevationImageRender(QObject* parent = nullptr);
		~ElevationImageRender();

	Q_SIGNALS:
		void renderingComplete();

	public Q_SLOTS:
		void render();

	public:
		void setViewport(QRect viewport);
		void setImage(std::shared_ptr<elevation::ElevationImage>);

	private:
		bool isGLInitialized() const;
		void initializeGL();
		void uploadImage(const elevation::ElevationImage&);
		void releaseCurrentImage();
		void bindCurrentTexture();
		void renderImage(const elevation::ElevationImage&, bool upload);

		std::unique_ptr<QOpenGLShaderProgram> m_program;
		std::shared_ptr<elevation::ElevationImage> m_image;
		std::mutex m_imageSync; // for synchronization as image is read on rendering thread, but can be changed on other thread.
		unsigned int m_imageHandle;
		bool m_imageChanged;
		QRect m_viewport;
		std::vector<int> m_uniformLocations;
	};
}