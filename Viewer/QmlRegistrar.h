#pragma once

#include <vector>

namespace viewer
{
	//QML specific constants
	constexpr char kQmlNamespace[] = "elevation.viewer";

	class QmlRegistrar
	{
	public:
		using RegistrarType = void(*)();

		static QmlRegistrar& instance();

		void add(RegistrarType registrar);
		void runRegistration();

	private:
		QmlRegistrar() = default;

		std::vector<RegistrarType> m_registrars;
	};
}

#define CREATE_QML_TYPE_REGISTER(EXPR) \
namespace \
{ \
	struct QmlAddToRegistrar_ \
	{ \
		QmlAddToRegistrar_() \
		{ \
			viewer::QmlRegistrar::instance().add([]() \
			{ \
				EXPR \
			}); \
		} \
	} g_adderInstance_;\
}